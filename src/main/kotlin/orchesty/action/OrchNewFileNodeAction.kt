package orchesty.action

import com.intellij.ide.fileTemplates.FileTemplateManager
import com.intellij.ide.fileTemplates.FileTemplateUtil
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.psi.PsiDirectory
import orchesty.OrchIcon
import orchesty.form.NewOrchestyNode
import orchesty.utils.Consts
import orchesty.utils.Strings
import orchesty.utils.TestFiles
import java.util.*

class OrchNewFileNodeAction : AnAction(OrchIcon.ORCHESTY), DumbAware {

    override fun update(event: AnActionEvent) {
        val presentation = event.getPresentation();
        presentation.setVisible(true);
        presentation.setEnabled(true);
    }

    override fun actionPerformed(e: AnActionEvent) {
        val dataContext: DataContext = e.dataContext;
        val view = LangDataKeys.IDE_VIEW.getData(dataContext) ?: return;
        val project: Project = PlatformDataKeys.PROJECT.getData(dataContext)!!;
        val dir = view.orChooseDirectory!!;

        val fileForm = NewOrchestyNode(project, dir.virtualFile.presentableUrl);
        fileForm.title = "New Orchesty Node";
        fileForm.pack();
        fileForm.show();

        if (fileForm.exitCode == DialogWrapper.OK_EXIT_CODE) {
            // TODO Validace
            val fileTemplate = FileTemplateManager.getInstance(project).getInternalTemplate(fileForm.template());
            val isTest = fileForm.template().endsWith("Test");

            val props = HashMap<String, Any>();
            val filename = fileForm.filename();
            val kebabName = fileForm.nodeName();

            props["POST_METHOD"] = fileForm.isPostMethod();
            props["METHOD"] = fileForm.method();
            props["NAME"] = filename;
            props["KEBAB_NAME"] = kebabName;
            props["UPPER_NAME"] = Strings.kebabToUpper(kebabName);
            props["ASYNC"] = fileForm.isAsync;

            if (isTest || fileForm.withTest()) {
                props["BEFORE_EACH"] = fileForm.withBeforeEach();
                props["AFTER_EACH"] = fileForm.withAfterEach();
                props["AFTER_ALL"] = fileForm.withAfterAll();

                val current = Strings.folders(dir.virtualFile.presentableUrl).count();
                val proj = Strings.folders(project.basePath!!).count();
                var repeat = current - proj;
                if (isTest) {
                    repeat -= 1;
                }

                if (repeat > 0) {
                    val path = Array(repeat) {
                        ".."
                    };
                    props["CONTAINER_PATH"] = path.joinToString("/") + "/test/TestAbstract";
                } else {
                    props["CONTAINER_PATH"] = "/test/TestAbstract";
                }
            }

            val properties = Properties(FileTemplateManager.getInstance(project).defaultProperties);
            properties.putAll(props);

            val createdElement = FileTemplateUtil.createFromTemplate(
                fileTemplate,
                filename,
                properties,
                dir,
            );

            var subDir: PsiDirectory? = null;
            if (!isTest && fileForm.withTest()) {
                val testTemplate = FileTemplateManager
                    .getInstance(project)
                    .getInternalTemplate(fileForm.template() + "Test");
                subDir = dir.findSubdirectory(Consts.TEST_DIRECTORY);
                if (subDir == null) {
                    WriteAction.run<Exception> {
                        subDir = dir.createSubdirectory(Consts.TEST_DIRECTORY);
                    };
                }

                FileTemplateUtil.createFromTemplate(
                    testTemplate,
                    filename,
                    properties,
                    subDir!!,
                );
            }

            view.selectElement(createdElement);

            if (isTest || fileForm.withTest()) {
                TestFiles.createTestFiles(
                    filename,
                    subDir?.virtualFile?.presentableUrl ?: dir.virtualFile.presentableUrl,
                    fileForm.template().startsWith(Consts.CONNECTOR),
                );
                dir.virtualFile.refresh(true, true);
            }
        }
    }

}

package orchesty.form

import orchesty.OrchIcon
import java.awt.Component
import javax.swing.JLabel
import javax.swing.JList
import javax.swing.ListCellRenderer

class TemplateItemRenderer : ListCellRenderer<Any> {

    override fun getListCellRendererComponent(
        list: JList<out Any>?,
        value: Any?,
        index: Int,
        isSelected: Boolean,
        cellHasFocus: Boolean
    ): Component {
        val text = value.toString();
        val lb = JLabel(text);
        if (text.endsWith("Test")) {
            lb.icon = OrchIcon.RUN_TEST;
        } else {
            lb.icon = OrchIcon.ORCHESTY;
        }

        return lb;
    }

}

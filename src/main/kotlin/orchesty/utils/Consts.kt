package orchesty.utils

object Consts {

    const val INPUT_TEST_FILE = "input.json";
    const val OUTPUT_TEST_FILE = "output.json";
    const val MOCK_TEST_FILE = "mock.json";

    val MANDATORY_TEST_FILES = arrayOf(INPUT_TEST_FILE, OUTPUT_TEST_FILE);
    val ALL_TEST_FILES = arrayOf(INPUT_TEST_FILE, OUTPUT_TEST_FILE, MOCK_TEST_FILE);

    const val CONNECTOR = "Connector";
    const val BATCH = "Batch";
    const val CUSTOM_NODE = "CustomNode";

    const val DATA_DIRECTORY = "Data";
    const val TEST_DIRECTORY = "__tests__";
}
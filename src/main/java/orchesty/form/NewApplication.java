package orchesty.form;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class NewApplication extends DialogWrapper {
    private JPanel panel1;
    private JTextField name;
    private JCheckBox webhook;

    public NewApplication(@Nullable Project project) {
        super(project);
        init();
    }

    public boolean webhook() {
        return webhook.isSelected();
    }

    public String name() {
        return name.getText();
    }

    @Override
    public @Nullable JComponent getPreferredFocusedComponent() {
        return name;
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return panel1;
    }

    @Override
    protected @Nullable ValidationInfo doValidate() {
        if (name.getText().isBlank()) {
            return new ValidationInfo(
                    "Name cannot be blank",
                    name
            );
        }

        return null;
    }

}

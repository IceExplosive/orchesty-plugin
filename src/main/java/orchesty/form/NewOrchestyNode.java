package orchesty.form;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import orchesty.utils.Consts;
import orchesty.utils.Strings;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.List;

public class NewOrchestyNode extends DialogWrapper {
    private JPanel panel1;
    private JTextField filename;
    private JCheckBox isAsync;
    private JComboBox template;
    private JCheckBox withTest;
    private JCheckBox withBeforeEach;
    private JCheckBox withAfterAll;
    private JCheckBox withAfterEach;
    private JTextField nodeName;
    public JRadioButton rbGet;
    public JRadioButton rbPost;
    public JRadioButton rbPut;
    public JRadioButton rbDelete;
    public JLabel methodLb;

    private final String expectedTemplate;

    public NewOrchestyNode(@Nullable Project project, String path) {
        super(project);
        template.setRenderer(new TemplateItemRenderer());
        init();
        filename.addKeyListener(new TemplateSelector(template));
        filename.getDocument().addDocumentListener(new FilenameDocumentListener(filename, nodeName));

        expectedTemplate = expectedTemplate(path);
        template.addItemListener(new TemplateChangeListener(withTest, this));
        presetTemplate();
    }

    public String template() {
        return this.template.getSelectedItem().toString();
    }

    public boolean isAsync() {
        return this.isAsync.isSelected();
    }

    public boolean withTest() {
        return this.withTest.isSelected();
    }

    public boolean withBeforeEach() {
        return this.withBeforeEach.isSelected();
    }

    public boolean withAfterAll() {
        return this.withAfterAll.isSelected();
    }

    public boolean withAfterEach() {
        return this.withAfterEach.isSelected();
    }

    public String filename() {
        return this.filename.getText();
    }

    public String nodeName() {
        return this.nodeName.getText();
    }

    public boolean isPostMethod() {
        return rbPost.isSelected() || rbPut.isSelected();
    }

    public String method() {
        if (rbPost.isSelected()) {
            return "POST";
        } else if (rbPut.isSelected()) {
            return "PUT";
        } else if (rbDelete.isSelected()) {
            return "DELETE";
        } else {
            return "GET";
        }
    }

    public void validateTemplate() {
        if (expectedTemplate != null) {
            if (!expectedTemplate.equals(template.getSelectedItem().toString())) {
                this.updateErrorInfo(
                        List.of(new ValidationInfo(
                                "Mismatched template with current folder",
                                template
                        ).asWarning().withOKEnabled())
                );
                return;
            }
        }

        this.updateErrorInfo(List.of());
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return panel1;
    }

    @Override
    public @Nullable JComponent getPreferredFocusedComponent() {
        return filename;
    }

    @Override
    protected @Nullable ValidationInfo doValidate() {
        if (filename.getText().isBlank()) {
            return new ValidationInfo(
                    "Name cannot be blank",
                    filename
            );
        }

        return null;
    }

    private void presetTemplate() {
        int count = template.getItemCount();
        for (int i = 0; i < count; i++) {
            if (expectedTemplate.equals(template.getItemAt(i).toString())) {
                template.setSelectedIndex(i);
                break;
            }
        }
    }

    private String expectedTemplate(String path) {
        List<String> folders = Strings.INSTANCE.folders(path);
        if (!folders.isEmpty()) {
            String folder = folders.get(folders.size() - 1);
            if (folder.equals(Consts.TEST_DIRECTORY)) {
                folder = folders.get(folders.size() - 2) + "Test";
            }

            return folder;
        }

        return null;
    }
}

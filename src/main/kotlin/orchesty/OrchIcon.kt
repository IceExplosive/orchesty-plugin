package orchesty

import com.intellij.openapi.util.IconLoader

object OrchIcon {
    val ORCHESTY = IconLoader.getIcon("icons/orchesty.svg", OrchIcon::class.java)
    val RUN_TEST = IconLoader.getIcon("icons/orchesty_run.svg", OrchIcon::class.java)
}

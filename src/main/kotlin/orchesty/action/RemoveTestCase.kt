package orchesty.action

import com.intellij.codeInsight.actions.ReformatCodeProcessor
import com.intellij.codeInsight.intention.impl.BaseIntentionAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import orchesty.psi.utils.MochaTestUtil
import orchesty.utils.Consts

class RemoveTestCase : BaseIntentionAction() {

    var it: PsiElement? = null;

    override fun getText(): String = "Orchesty - Remove test case with .json files"

    override fun getFamilyName(): String = "Remove test"

    override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?): Boolean {
        if (editor === null || file === null) {
            return false;
        }

        val el = file.findElementAt(editor.caretModel.offset);
        it = MochaTestUtil.findParentIt(el)?.parent;

        return it != null;
    }

    override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
        if (file === null || editor === null || it == null) {
            return;
        }

        try {
            it!!.delete();
            ReformatCodeProcessor(file, false);

            val dir = file.containingDirectory
                .findSubdirectory(Consts.DATA_DIRECTORY)
                ?.findSubdirectory(file.name.split(".").first())
                ?: return;

            var prefix = "";

            val regex =
                Regex("await\\s+tester.test(${Consts.CONNECTOR}|${Consts.BATCH}|${Consts.CUSTOM_NODE})\\([a-zA-Z_-]+,\\s*['\"]{1}(.+)['\"]");
            val match = regex.find(it!!.text);
            if (match != null) {
                prefix = "${match.groupValues.last()}-";
            }

            Consts.ALL_TEST_FILES.forEach {
                dir.findFile("$prefix$it")?.delete();
            }
        } catch (e: Exception) {
            // ignore
        }
    }

}

package orchesty.action

import com.intellij.codeInsight.actions.ReformatCodeProcessor
import com.intellij.codeInsight.intention.impl.BaseIntentionAction
import com.intellij.lang.javascript.psi.JSObjectLiteralExpression
import com.intellij.lang.javascript.psi.ecma6.TypeScriptInterface
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.EditorModificationUtil
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset

class IgnoreNamingConventionsAction : BaseIntentionAction() {

    companion object {
        val LINT_NAME = "@typescript-eslint/naming-convention"
    }

    var tsInterface: PsiElement? = null;
    var tsLiteral: PsiElement? = null;

    override fun getFamilyName(): String = "Ignore naming conventions";

    override fun getText(): String = "Ignore naming conventions";

    override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?): Boolean {
        if (editor === null || file === null) {
            return false;
        }

        tsInterface = PsiTreeUtil.getParentOfType(
            file.findElementAt(editor.caretModel.offset),
            TypeScriptInterface::class.java
        );
        if (tsInterface != null) return true

        tsLiteral = PsiTreeUtil.getParentOfType(
            file.findElementAt(editor.caretModel.offset),
            JSObjectLiteralExpression::class.java
        );

        return tsLiteral != null;
    }

    override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
        if (editor === null || file === null) {
            return;
        }

        val caret = editor.caretModel;
        val position = editor.caretModel.offset;
        val enableText = "\n/* eslint-enable $LINT_NAME */";
        val disableText = "/* eslint-disable $LINT_NAME */\n";

        if (tsInterface != null) {
            caret.moveToOffset(tsInterface!!.endOffset);
            EditorModificationUtil.insertStringAtCaret(editor, enableText);
            caret.moveToOffset(tsInterface!!.startOffset);
            EditorModificationUtil.insertStringAtCaret(editor, disableText);
            caret.moveToOffset(position + enableText.length + 1);
            PsiDocumentManager.getInstance(project).commitDocument(editor.document);
            ReformatCodeProcessor(file, true).run();
            return;
        }

        if (tsLiteral != null) {
            val endOffset = editor.document.text.indexOf("\n", tsLiteral!!.endOffset) + 1;
            caret.moveToOffset(endOffset);
            EditorModificationUtil.insertStringAtCaret(editor, enableText);

            val startOffset = editor.document.text.substring(0, tsLiteral!!.startOffset).lastIndexOf("\n");
            caret.moveToOffset(startOffset);
            EditorModificationUtil.insertStringAtCaret(editor, disableText);

            caret.moveToOffset(position + enableText.length + 1);
            PsiDocumentManager.getInstance(project).commitDocument(editor.document);
            ReformatCodeProcessor(file, true).run();
        }
    }

}
package orchesty.utils

import com.intellij.lang.javascript.psi.JSVarStatement
import com.intellij.lang.javascript.psi.ecma6.TypeScriptVariable
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiNamedElement
import com.intellij.psi.util.PsiTreeUtil

object TsVariableUtils {

    fun getVariableByName(root: PsiElement, name: String): TypeScriptVariable? {
        val variables = PsiTreeUtil.getChildrenOfType(root, JSVarStatement::class.java);
        variables?.forEach {
            val variable = PsiTreeUtil.getChildOfType(it as PsiElement, TypeScriptVariable::class.java);
            if (variable != null) {
                val el = variable as PsiNamedElement;
                if (el.name == name) {
                    return variable;
                }
            }
        }

        return null;
    }

}
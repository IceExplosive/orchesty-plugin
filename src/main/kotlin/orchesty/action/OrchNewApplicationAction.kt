package orchesty.action

import com.intellij.ide.fileTemplates.FileTemplateManager
import com.intellij.ide.fileTemplates.FileTemplateUtil
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import orchesty.OrchIcon
import orchesty.form.NewApplication
import orchesty.utils.Strings
import java.util.*

class OrchNewApplicationAction : AnAction(OrchIcon.ORCHESTY), DumbAware {

    override fun update(event: AnActionEvent) {
        val presentation = event.getPresentation();
        presentation.setVisible(true);
        presentation.setEnabled(true);
    }

    override fun actionPerformed(e: AnActionEvent) {
        val dataContext: DataContext = e.dataContext;
        val view = LangDataKeys.IDE_VIEW.getData(dataContext) ?: return;
        val project: Project = PlatformDataKeys.PROJECT.getData(dataContext)!!;
        val dir = view.orChooseDirectory!!;

        val fileForm = NewApplication(project);
        fileForm.title = "New Orchesty Application";
        fileForm.pack();
        fileForm.show();

        if (fileForm.exitCode == DialogWrapper.OK_EXIT_CODE) {
            val fileTemplate = FileTemplateManager.getInstance(project).getInternalTemplate("Application");

            val props = HashMap<String, Any>();
            val filename = "${fileForm.name()}Application";
            val kebabName = Strings.toKebab(fileForm.name());

            props["NAME"] = fileForm.name();
            props["KEBAB_NAME"] = kebabName;
            props["PUBLIC_NAME"] = Strings.toSpaced(fileForm.name());
            props["WEBHOOK"] = fileForm.webhook();
            props["IMPLS"] = "";

            val impls: MutableList<String> = mutableListOf();
            if (fileForm.webhook()) {
                impls.add("IWebhookApplication");
            }
            if (impls.isNotEmpty()) {
                props["IMPLS"] = "implements ${impls.joinToString(", ")}";
            }

            val properties = Properties(FileTemplateManager.getInstance(project).defaultProperties);
            properties.putAll(props);

            val createdElement = FileTemplateUtil.createFromTemplate(
                fileTemplate,
                filename,
                properties,
                dir,
            );

            view.selectElement(createdElement);
        }
    }

}

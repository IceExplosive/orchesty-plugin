package orchesty.form

import orchesty.utils.Strings
import javax.swing.JTextField
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener

class FilenameDocumentListener(private val nameField: JTextField, private val nodeName: JTextField) : DocumentListener {

    override fun insertUpdate(e: DocumentEvent?) {
        update();
    }

    override fun removeUpdate(e: DocumentEvent?) {
        update();
    }

    override fun changedUpdate(e: DocumentEvent?) {
        update();
    }

    private fun update() {
        nodeName.text = Strings.toKebab(nameField.text);
    }

}
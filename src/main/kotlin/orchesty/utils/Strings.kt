package orchesty.utils

object Strings {

    fun toKebab(value: String): String =
        value
            .replace(Regex("([a-z0-9])([A-Z])"), "$1-$2")
            .replace(Regex("([a-zA-Z])([0-9])"), "$1-$2")
            .replace(Regex("([A-Z])([A-Z])(?=[a-z])"), "$1-$2")
            .lowercase();

    fun kebabToUpper(value: String): String = value.replace('-', '_').uppercase();

    fun toSpaced(value: String): String =
        value
            .replace(Regex("([a-z0-9])([A-Z])"), "$1 $2")
            .replace(Regex("([a-zA-Z])([0-9])"), "$1 $2")
            .replace(Regex("([A-Z])([A-Z])(?=[a-z])"), "$1 $2");

    fun folders(path: String): List<String> = path.split("/", "\\");

}
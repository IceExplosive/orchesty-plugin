package orchesty.form

import java.awt.event.ItemEvent
import java.awt.event.ItemListener
import javax.swing.JCheckBox

class TemplateChangeListener(
    private val withTest: JCheckBox,
    private val dialog: NewOrchestyNode,
) : ItemListener {

    override fun itemStateChanged(e: ItemEvent?) {
        val value = e?.item.toString()
        val enabled = value.endsWith("Test")

        withTest.isEnabled = !enabled
        dialog.validateTemplate()
        enableRbs(value == "Connector")
    }

    private fun enableRbs(enable: Boolean) {
        dialog.rbGet.isEnabled = enable
        dialog.rbPost.isEnabled = enable
        dialog.rbPut.isEnabled = enable
        dialog.rbDelete.isEnabled = enable
    }

}

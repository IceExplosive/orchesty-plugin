package orchesty.action

import com.intellij.codeInsight.actions.ReformatCodeProcessor
import com.intellij.codeInsight.actions.SimpleCodeInsightAction
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.EditorModificationUtil
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.popup.JBPopup
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.refactoring.suggested.endOffset
import orchesty.OrchIcon
import orchesty.form.NewOrchNodeTest
import orchesty.psi.utils.MochaTestUtil
import orchesty.utils.TestFiles
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.lang.Exception
import javax.swing.BorderFactory
import javax.swing.JPanel

class OrchNewTestAction : SimpleCodeInsightAction() {

    init {
        val presentation = templatePresentation;
        presentation.icon = OrchIcon.ORCHESTY;
    }

    override fun invoke(project: Project, editor: Editor, file: PsiFile) {
        if (!EditorModificationUtil.checkModificationAllowed(editor)) return;

        var withMock = false;

        val doc = file.viewProvider.document;
        var offset = doc.text.indexOf("beforeEach");
        offset = doc.text.indexOf("NodeTester", offset);
        var psi = file.findElementAt(offset)!!.parent;
        psi = psi?.nextSibling;

        if (psi?.children?.size ?: 0 >= 2) {
            withMock = psi?.lastChild?.prevSibling?.text ?: "" == "true";
        }

        val form = NewOrchNodeTest(project);
        val pnl = JPanel();
        pnl.border = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        pnl.add(form.panel1);

        form.setWithMockfile(withMock);

        val pop = JBPopupFactory.getInstance()
            .createComponentPopupBuilder(pnl, form.prefix)
            .setMovable(true)
            .setFocusable(true)
            .setRequestFocus(true)
            .setResizable(true)
            .createPopup();

        val adapter = NewTestKeyAdapter(pop, form, project, editor, file);
        form.prefix.addKeyListener(adapter);
        form.withMock.addKeyListener(adapter);

        pop.showInBestPositionFor(editor);
    }

    override fun isValidForFile(project: Project, editor: Editor, file: PsiFile): Boolean {
        return file.name.endsWith(".ts");
    }
}

private class NewTestKeyAdapter(
    val popUp: JBPopup,
    val form: NewOrchNodeTest,
    val project: Project,
    val editor: Editor,
    val file: PsiFile,
) : KeyAdapter() {

    override fun keyPressed(e: KeyEvent?) {
        if (e?.keyCode == KeyEvent.VK_ENTER) {
            WriteCommandAction.writeCommandAction(project, file).run<Exception> {
                var el = file.findElementAt(editor.caretModel.offset) ?: file.firstChild;

                el = MochaTestUtil.findParentDescribe(el);
                if (el == null) { // Check also downwards should caret be outside of description scope
                    el = PsiTreeUtil.collectElements(file) {
                        MochaTestUtil.isDescribe(it)
                    }.lastOrNull();
                }

                if (el != null) {
                    el = MochaTestUtil.endingOffsetOfDescribe(el);
                    val pos = el?.endOffset ?: 0;

                    if (pos > 0) {
                        val toInsert = formTest(form.prefix(), editor, file);
                        val toOffset = toInsert.indexOf(");");

                        editor.caretModel.moveToOffset(pos);
                        EditorModificationUtil.insertStringAtCaret(editor, toInsert);
                        PsiDocumentManager.getInstance(project).commitDocument(editor.document);
                        editor.caretModel.moveToOffset(pos + toOffset + 2);

                        ReformatCodeProcessor(file, false).run();

                        val dirPath = file.virtualFile.presentableUrl;
                        TestFiles.createTestFiles(
                            file.name.split(".").first(),
                            dirPath.substring(0, dirPath.lastIndexOfAny(listOf("\\", "/"))),
                            form.withMockfile(),
                            form.prefix(),
                        );
                        file.containingDirectory.virtualFile.refresh(true, true);
                    }
                }
            }
            popUp.dispose();
        }
    }

    private fun formTest(prefix: String, editor: Editor, file: PsiFile): String {
        val dir = file.containingDirectory.parentDirectory!!.name; // TODO změnit na parse dle typu Class
        val reg = Regex("import\\s+\\{\\s*NAME\\s+as\\s+([a-zA-Z_\\-]+)\\s*}\\s+from\\s+'\\.\\./");
        val match = reg.find(editor.document.text);
        val name = match?.groups?.last()?.value ?: "";

        return """
        it('process${ if (prefix.isBlank()) { "" } else { " - $prefix" } }', async () => {
            await tester.test$dir($name${ if (prefix.isBlank()) { "" } else { ", '$prefix'" } });
        });
        
        
        """.trimIndent();
    }

}

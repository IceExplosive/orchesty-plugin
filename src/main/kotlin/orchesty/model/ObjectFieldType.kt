package orchesty.model

enum class ObjectFieldType {
    OBJECT,
    ARRAY,
    TYPE,
}
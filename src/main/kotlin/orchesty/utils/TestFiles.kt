package orchesty.utils

import java.io.File

object TestFiles {

    private val SEP = File.separator;

    fun createTestFiles(name: String, directory: String, withMock: Boolean, prefix: String? = null) {
        val parent = "$directory${SEP}${Consts.DATA_DIRECTORY}$SEP${name}";
        val pre = if (prefix != null) "$prefix-" else "";
        File(parent).mkdirs();

        Consts.MANDATORY_TEST_FILES.forEach {
            val file = File("$parent$SEP$pre$it");
            file.createNewFile();
            file.appendText(
                """{
    "headers": {},
    "data": {}
}

""".trimIndent()
            );
        };

        if (withMock) {
            val file = File("$parent$SEP${pre}${Consts.MOCK_TEST_FILE}");
            file.createNewFile();
            file.appendText(
                """{
  "http": "POST http://url.com",
  "code": 200,
  "body": {}
}

""".trimIndent()
            );
        };
    }

}
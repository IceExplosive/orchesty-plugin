package orchesty.formatter

import com.intellij.lang.ASTNode
import com.intellij.lang.folding.FoldingBuilderEx
import com.intellij.lang.folding.FoldingDescriptor
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.FoldingGroup
import com.intellij.openapi.project.DumbAware
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiNamedElement
import com.intellij.util.containers.toArray
import orchesty.Orchesty
import orchesty.utils.TsVariableUtils


class JoiSchemaFoldingBuilder : FoldingBuilderEx(), DumbAware {

    override fun buildFoldRegions(root: PsiElement, document: Document, quick: Boolean): Array<FoldingDescriptor> {
        val group = FoldingGroup.newGroup("Orchesty");
        val descriptors: MutableList<FoldingDescriptor> = ArrayList();

        val variable = TsVariableUtils.getVariableByName(root, Orchesty.INPUT_SCHEMA);
        if (variable != null) {
            val el = variable as PsiNamedElement;
            descriptors.add(FoldingDescriptor(el.node, el.textRange, group));
        }

        return descriptors.toArray(emptyArray());
    }

    override fun getPlaceholderText(node: ASTNode): String? {
        return Orchesty.INPUT_SCHEMA;
    }

    override fun isCollapsedByDefault(node: ASTNode): Boolean {
        return true;
    }

}
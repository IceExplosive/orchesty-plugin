package orchesty.model

import com.intellij.lang.javascript.psi.JSArgumentList
import com.intellij.lang.javascript.psi.JSArrayLiteralExpression
import com.intellij.lang.javascript.psi.JSCallExpression
import com.intellij.lang.javascript.psi.JSObjectLiteralExpression
import com.intellij.lang.javascript.psi.JSProperty
import com.intellij.lang.javascript.psi.JSType
import com.intellij.lang.javascript.psi.ecma6.*
import com.intellij.lang.javascript.psi.ecma6.impl.TypeScriptInterfaceImpl
import com.intellij.lang.javascript.psi.ecmal4.JSClass
import com.intellij.lang.javascript.psi.types.JSArrayType
import com.intellij.lang.javascript.psi.types.JSSimpleRecordTypeImpl
import com.intellij.lang.javascript.psi.types.JSTypeImpl
import com.intellij.lang.javascript.psi.types.primitives.JSNullType
import com.intellij.lang.javascript.psi.types.primitives.JSNumberType
import com.intellij.lang.javascript.psi.types.primitives.JSStringType
import com.intellij.lang.javascript.psi.types.primitives.JSUndefinedType
import com.intellij.psi.PsiElement
import com.intellij.psi.util.PsiTreeUtil

/** TODO
 * Joi.array() -> možnost required
 * Joi.object() -> možnost required
 */

data class ObjectField(
    val key: String,
    var type: ObjectFieldType,
    var nullable: Boolean,
    val children: MutableList<ObjectField>
) {

    companion object {

        /** From Interface */

        fun fromInterface(element: TypeScriptInterface): ObjectField {
            val fields = (element.fields.toMutableList()) as MutableList<TypeScriptPropertySignature>;
            val parents: MutableList<JSClass> = element.superClasses.toMutableList();
            while (parents.isNotEmpty()) {
                val parent = parents.removeAt(0);
                fields.addAll(parent.fields.toList() as List<TypeScriptPropertySignature>);
                parents.addAll(parent.superClasses);
            }

            return fromTSFields(element.name ?: "_root", fields);
        }

        private fun fromTSFields(rootName: String, fields: List<TypeScriptPropertySignature>): ObjectField {
            val root = ObjectField(rootName, ObjectFieldType.OBJECT, false, mutableListOf());
            addFields(root, fields);

            return root;
        }

        private fun addFields(field: ObjectField, fields: List<TypeScriptPropertySignature>) {
            field.children.addAll(fields.map {
                var name = it.name ?: "";
                if (name.contains("-") || name.first().isDigit()) {
                    name = "'$name'";
                }

                val child = ObjectField(name, ObjectFieldType.TYPE, it.isOptional, mutableListOf());
                processDeclaration(child, it.typeDeclaration!!);
                child
            });
        }

        private fun processDeclaration(
            field: ObjectField,
            declaration: JSTypeDeclaration
        ) {
            when (declaration) {
                is TypeScriptSingleType -> {
                    processType(field, declaration.jsType);
                }

                is TypeScriptUnionOrIntersectionType -> {
                    declaration.types.forEach {
                        processDeclaration(field, it);
                    }
                }

                is TypeScriptArrayType -> {
                    field.type = ObjectFieldType.ARRAY;
                    val type = declaration.jsType as JSArrayType;
                    processType(field, type.type!!);
                }

                is TypeScriptObjectType -> {
                    field.type = ObjectFieldType.OBJECT;
                    val members = declaration.typeMembers.asList() as List<TypeScriptPropertySignature>;
                    addFields(field, members);
                }
            }
        }

        private fun processType(
            field: ObjectField,
            type: JSType,
        ) {
            var f = field;
            when (type) {
                is JSNullType, is JSUndefinedType -> {
                    field.nullable = true;
                }

                is JSStringType, is JSNumberType -> {
                    field.children.add(
                        ObjectField(type.typeText, ObjectFieldType.TYPE, false, mutableListOf())
                    );
                }

                is JSSimpleRecordTypeImpl -> {
                    val child = ObjectField(field.key, ObjectFieldType.OBJECT, false, mutableListOf());
                    field.children.add(child);
                    addFields(child, type.typeMembers.toList() as List<TypeScriptPropertySignature>);
                }

                is JSTypeImpl -> {
                    when (val declaration = type.declarations.first()) {
                        is TypeScriptInterfaceImpl -> {
                            if (field.type == ObjectFieldType.ARRAY) {
                                val arrayType = ObjectField(field.key, ObjectFieldType.TYPE, false, mutableListOf());
                                field.children.add(arrayType);
                                f = arrayType;
                            }

                            f.type = ObjectFieldType.OBJECT;
                            addFields(f, declaration.fields.toList() as List<TypeScriptPropertySignature>);
                        }

                        is TypeScriptTypeAlias -> {
                            processDeclaration(field, declaration.typeDeclaration!!);
                        }
                    }
                }
            }
        }

        /** From Joi */

        fun fromJoi(element: TypeScriptVariable): ObjectField {
            val call = (element as PsiElement).lastChild as JSCallExpression;
            val root = ObjectField(element.name ?: "_root", ObjectFieldType.OBJECT, false, mutableListOf());
            addFromJoiObject(root, call);

            return root;
        }

        private fun addFromJoiObject(field: ObjectField, reference: JSCallExpression) {
            val args = reference.argumentList ?: return;
            val literal = PsiTreeUtil.getChildOfType(args, JSObjectLiteralExpression::class.java) ?: return;

            val props = PsiTreeUtil.getChildrenOfType(literal, JSProperty::class.java);
            props?.forEach {
                addJoiField(field, it);
            }
        }

        private fun addJoiField(parent: ObjectField, prop: JSProperty) {
            var name = prop.name ?: "";
            if (name.isNotEmpty() && (name.contains("-") || name.first().isDigit())) {
                name = "'$name'";
            }

            val field = ObjectField(name, ObjectFieldType.TYPE, true, mutableListOf());
            parent.children.add(field);
            when (val last = prop.lastChild) {
                is JSCallExpression -> {
                    addJoiType(field, last);
                }
                is JSArrayLiteralExpression -> {
                    val calls = PsiTreeUtil.getChildrenOfType(last, JSCallExpression::class.java);
                    calls?.forEach {
                        addJoiType(field, it);
                    }
                }
            }
        }

        private fun addJoiType(parent: ObjectField, expr: JSCallExpression) {
            val text: String = expr.text;
            if (text.startsWith("Joi.object")) {
                parent.type = ObjectFieldType.OBJECT;
                parent.nullable = !isJoiRequired(text);

                var innerExpr: PsiElement? = expr as PsiElement;
                var innerText = text;
                while (innerExpr != null && innerText != "Joi.object") {
                    innerExpr = innerExpr.firstChild;
                    innerText = innerExpr.text;
                }

                addFromJoiObject(parent, (innerExpr?.parent ?: expr) as JSCallExpression);
            } else if (text.startsWith("Joi.array")) {
                parent.type = ObjectFieldType.ARRAY;
                parent.nullable = !isJoiRequired(text);

                var innerExpr: PsiElement? = expr as PsiElement;
                var innerText = text.replace(" ", "").replace("\n", "");
                while (innerExpr != null && innerText != "Joi.array().items") {
                    innerExpr = innerExpr.firstChild;
                    innerText = innerExpr.text.replace(" ", "").replace("\n", "");
                }

                (innerExpr?.nextSibling as JSArgumentList?)?.arguments?.forEach {
                    if (it is JSCallExpression) {
                        var current = parent;
                        if (it.text.startsWith("Joi.object")) {
                            current = ObjectField(parent.key, ObjectFieldType.OBJECT, true, mutableListOf());
                            parent.children.add(current);
                        }
                        addJoiType(current, it);
                    }
                }
            } else {
                if (text.contains(".number()")) {
                    parent.children.add(ObjectField("number", ObjectFieldType.TYPE, true, mutableListOf()))
                }
                if (text.contains(".string()")) {
                    parent.children.add(ObjectField("string", ObjectFieldType.TYPE, true, mutableListOf()))
                }
                if (text.contains(".required()")) {
                    parent.nullable = false;
                }
            }
        }

        private fun isJoiRequired(text: String): Boolean {
            var toSkip = 0;
            var index = text.indexOf("()");
            while (index < text.length) {
                val open = text.indexOf("(", index);
                val close = text.indexOf(")", index);
                if (open <= close) {
                    toSkip += 1;
                    index = open + 1;
                } else {
                    toSkip -= 1;
                    index = close + 1;
                }

                if (toSkip <= 0) {
                    break;
                }
            }

            return text.substring(index).contains("required");
        }
    }

    /** Into Joi */

    fun intoJoi(): StringBuilder {
        val builder = StringBuilder();
        builder.append("const inputSchema = ");
        appendJoiObject(builder, ";", true);

        return builder;
    }

    private fun appendJoiObject(builder: StringBuilder, ending: String = ",", omitRequired: Boolean = false) {
        builder.appendLine("Joi.object({");
        children.forEach {
            builder.append("${it.key}: ");
            it.appendJoiTypes(builder);
        }
        builder.appendLine("})${if (!nullable && !omitRequired) ".required()" else ""}$ending");
    }

    private fun appendJoiArray(builder: StringBuilder, ending: String = ",") {
        builder.appendLine("Joi.array().items(");
        children.forEach {
            it.appendJoiTypes(builder);
        }
        builder.appendLine(")${if (!nullable) "\n.required()" else ""}$ending");
    }

    private fun appendJoiTypes(builder: StringBuilder) {
        when (this.type) {
            ObjectFieldType.OBJECT -> {
                appendJoiObject(builder);
            }

            ObjectFieldType.ARRAY -> {
                appendJoiArray(builder);
            }

            ObjectFieldType.TYPE -> {
                if (children.size == 1) {
                    builder.appendLine(
                        "${
                            children.first().joiType(false)
                        }${
                            if (!this.nullable) ".required()" else ""
                        },"
                    );
                } else if (children.size > 1) {
                    builder.appendLine("[");
                    children.forEach { builder.appendLine(it.joiType()) };
                    if (!this.nullable) {
                        builder.appendLine("Joi.required(),");
                    }
                    builder.appendLine("],");
                } else {
                    builder.appendLine(joiType());
                }
            }
        }
    }

    private fun joiType(comma: Boolean = true): String {
        return "Joi.${this.key}()${if (comma) "," else ""}";
    }

    /** Ord func */

    override fun equals(other: Any?): Boolean {
        if (other !is ObjectField) {
            return super.equals(other);
        }

        sortChildren();
        other.sortChildren();

        val myEn = encode(StringBuilder()).toString();
        val otherEn = other.encode(StringBuilder()).toString();

        return myEn == otherEn;
    }

    private fun sortChildren() {
        children.sortBy { it.key }
        children.forEach {
            it.sortChildren();
        }
    }

    private fun encode(builder: StringBuilder): StringBuilder {
        children.forEach {
            builder.appendLine("${it.key}:${it.type}:${if (it.nullable && it.children.size > 0) "req" else "null"}");
            it.encode(builder);
        }

        return builder;
    }

}

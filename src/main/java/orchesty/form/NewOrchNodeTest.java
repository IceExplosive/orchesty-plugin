package orchesty.form;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class NewOrchNodeTest extends DialogWrapper {
    public JTextField prefix;
    public JPanel panel1;
    public JCheckBox withMock;

    public NewOrchNodeTest(@Nullable Project project) {
        super(project);
        init();
    }

    public String prefix() {
        return prefix.getText();
    }

    public boolean withMockfile() {
        return withMock.isSelected();
    }

    public void setWithMockfile(boolean withMock) {
        this.withMock.setSelected(withMock);
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return panel1;
    }

    @Override
    public @Nullable JComponent getPreferredFocusedComponent() {
        return prefix;
    }

}

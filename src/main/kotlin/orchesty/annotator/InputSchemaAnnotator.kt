package orchesty.annotator

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.lang.javascript.psi.ecma6.ES6Decorator
import com.intellij.psi.PsiElement
import orchesty.Orchesty
import orchesty.model.ObjectField
import orchesty.utils.TsInterfaceUtils
import orchesty.utils.TsVariableUtils

class InputSchemaAnnotator : Annotator {

    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        if (element !is ES6Decorator) {
            return;
        }
        if (element.decoratorName != Orchesty.VALIDATE_DECORATOR) {
            return;
        }

        val file = element.containingFile;
        // TODO inputScheme & IInput je hard-coded
        val variable = TsVariableUtils.getVariableByName(file, Orchesty.INPUT_SCHEMA) ?: return;
        val inputType = TsInterfaceUtils.getInterfaceByName(file, Orchesty.INPUT_INTERFACE) ?: return;

        val inputObj = ObjectField.fromInterface(inputType);
        val schemaObj = ObjectField.fromJoi(variable);

        if (inputObj != schemaObj) {
            holder
                .newAnnotation(HighlightSeverity.ERROR, "Mismatch of validation with input schema")
                .range(element.textRange)
                .create();
        }
    }

}
import CoreFormsEnum from '@orchesty/nodejs-sdk/dist/lib/Application/Base/CoreFormsEnum';
import AProcessDto from '@orchesty/nodejs-sdk/dist/lib/Utils/AProcessDto';
import Form from '@orchesty/nodejs-sdk/dist/lib/Application/Model/Form/Form';
import FormStack from '@orchesty/nodejs-sdk/dist/lib/Application/Model/Form/FormStack';
import { HttpMethods } from '@orchesty/nodejs-sdk/dist/lib/Transport/HttpMethods';
import RequestDto from '@orchesty/nodejs-sdk/dist/lib/Transport/Curl/RequestDto';
import ResponseDto from '@orchesty/nodejs-sdk/dist/lib/Transport/Curl/ResponseDto';
import { ABasicApplication } from '@orchesty/nodejs-sdk/dist/lib/Authorization/Type/Basic/ABasicApplication';
import { ApplicationInstall } from '@orchesty/nodejs-sdk/dist/lib/Application/Database/ApplicationInstall';
import { CommonHeaders, JSON_TYPE } from '@orchesty/nodejs-sdk/dist/lib/Utils/Headers';
#if (${WEBHOOK})
import { IWebhookApplication } from '@orchesty/nodejs-sdk/dist/lib/Application/Base/IWebhookApplication';
#end
#if (${WEBHOOK})
import WebhookSubscription from '@orchesty/nodejs-sdk/dist/lib/Application/Model/Webhook/WebhookSubscription';
#end

export class ${NAME}Application extends ABasicApplication ${IMPLS}{

    public getName(): string {
        return '${KEBAB_NAME}';
    }

    public getPublicName(): string {
        return '${PUBLIC_NAME}';
    }

    public getDescription(): string {
        return '${PUBLIC_NAME} description';
    }

    public getFormStack(): FormStack {
        const form = new Form(CoreFormsEnum.AUTHORIZATION_FORM, 'Authorization settings');

        return new FormStack().addForm(form);
    }

    public getRequestDto(
        dto: AProcessDto,
        applicationInstall: ApplicationInstall,
        method: HttpMethods,
        url?: string,
        data?: unknown,
    ): RequestDto {
        const request = new RequestDto(url ?? '', method, dto);
        request.setHeaders({
            [CommonHeaders.CONTENT_TYPE]: JSON_TYPE,
            [CommonHeaders.ACCEPT]: JSON_TYPE,
        });

        if (data) {
            request.setJsonBody(data)
        }

        return request;
    }
#if (${WEBHOOK})

    public getWebhookSubscribeRequestDto(
        applicationInstall: ApplicationInstall,
        subscription: WebhookSubscription,
        url: string,
    ): RequestDto {
        // TODO unimplemented
        throw new Error('Unimplemented');
    }

    public getWebhookSubscriptions(): WebhookSubscription[] {
        return [];
    }

    public getWebhookUnsubscribeRequestDto(
        applicationInstall: ApplicationInstall,
        id: string,
    ): RequestDto {
        // TODO unimplemented
        throw new Error('Unimplemented');
    }

    public processWebhookSubscribeResponse(
        dto: ResponseDto,
        applicationInstall: ApplicationInstall,
    ): string {
        // TODO unimplemented
        throw new Error('Unimplemented');
    }

    public processWebhookUnsubscribeResponse(dto: ResponseDto): boolean {
        return dto.responseCode === 200;
    }
#end

}

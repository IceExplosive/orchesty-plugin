package orchesty.action

import com.intellij.codeInsight.actions.ReformatCodeProcessor
import com.intellij.codeInsight.intention.impl.BaseIntentionAction
import com.intellij.lang.javascript.psi.ecma6.TypeScriptClassExpression
import com.intellij.lang.javascript.psi.ecma6.TypeScriptFunction
import com.intellij.lang.javascript.psi.ecma6.TypeScriptInterface
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.EditorModificationUtil
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.refactoring.suggested.startOffset
import orchesty.Orchesty
import orchesty.model.ObjectField
import orchesty.utils.TsImportUtils

class CreateValidationAction : BaseIntentionAction() {

    var it: TypeScriptInterface? = null;

    override fun getFamilyName(): String = "Add Joi object";

    override fun getText(): String = "Creates Joi validation object";

    override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?): Boolean {
        if (editor === null || file === null) {
            return false;
        }

        it = PsiTreeUtil.getParentOfType(
            file.findElementAt(editor.caretModel.offset),
            TypeScriptInterface::class.java
        );

        return it != null;
    }

    override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
        if (editor === null || file === null || it == null) {
            return;
        }

        val tsParsed = ObjectField.fromInterface(it!!);

        val classElement: PsiElement =
            PsiTreeUtil.findChildOfType(file, TypeScriptClassExpression::class.java) ?: return;
        var rootPosition = classElement;
        while (rootPosition.parent !is PsiFile) {
            rootPosition = rootPosition.parent;
        }

        val functions = PsiTreeUtil.findChildrenOfType(rootPosition, TypeScriptFunction::class.java);
        var hasDecorator = false;
        for (function in functions) {
            if (function.name == "processAction") {
                for (attr in function.attributeList?.decorators ?: emptyArray()) {
                    if (attr.decoratorName == Orchesty.VALIDATE_DECORATOR) {
                        hasDecorator = true;
                        break;
                    }
                }

                if (!hasDecorator) {
                    val el = function as PsiElement;
                    editor.caretModel.moveToOffset(el.startOffset);
                    EditorModificationUtil.insertStringAtCaret(editor, "@validate(inputSchema)\n");
                }
            }
        }

        editor.caretModel.moveToOffset(rootPosition.startOffset);
        EditorModificationUtil.insertStringAtCaret(editor, tsParsed.intoJoi().appendLine().toString());

        TsImportUtils.addImportBinding("Joi", "import Joi from 'joi';", editor, file);

        if (hasDecorator) {
            TsImportUtils.addNamedImport(
                Orchesty.VALIDATE_DECORATOR,
                "import { validate } from '@orchesty/nodejs-sdk/dist/lib/Utils/Validations';",
                editor,
                file
            );
        }

        PsiDocumentManager.getInstance(project).commitDocument(editor.document);
        ReformatCodeProcessor(file, false).run();

        return;
    }

}

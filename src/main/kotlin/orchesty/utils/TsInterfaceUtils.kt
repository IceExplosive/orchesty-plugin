package orchesty.utils

import com.intellij.lang.javascript.psi.ecma6.TypeScriptInterface
import com.intellij.psi.PsiElement
import com.intellij.psi.util.PsiTreeUtil

object TsInterfaceUtils {

    fun getInterfaceByName(root: PsiElement, name: String): TypeScriptInterface? {
        val interfaces = PsiTreeUtil.getChildrenOfType(root, TypeScriptInterface::class.java);
        interfaces?.forEach {
            if (it.name == name) {
                return it;
            }
        }

        return null;
    }

}

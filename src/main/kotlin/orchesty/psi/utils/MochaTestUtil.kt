package orchesty.psi.utils

import com.intellij.psi.PsiElement
import com.intellij.psi.util.elementType
import com.intellij.psi.util.prevLeaf

object MochaTestUtil {

    fun endingOffsetOfDescribe(element: PsiElement?): PsiElement? {
        var el = element;
        el = el?.lastChild;
        el = findChild(el, "JS:TYPESCRIPT_FUNCTION_EXPRESSION");
        el = findChild(el, "BLOCK_STATEMENT");
        el = el?.lastChild?.prevLeaf(true);

        return el;
    }

    fun findParentDescribe(element: PsiElement?): PsiElement? {
        var el = element;
        while (el != null) {
            if (isDescribe(el)) {
                return el;
            }

            el = el.parent;
        }

        return null;
    }

    fun isDescribe(element: PsiElement?): Boolean {
        return element.elementType.toString() == "JS:CALL_EXPRESSION"
                && element?.text?.startsWith("describe(") ?: false;
    }

    fun isIt(element: PsiElement?): Boolean {
        return element.elementType.toString() == "JS:CALL_EXPRESSION"
                && element?.text?.startsWith("it(") ?: false;
    }

    fun findParentIt(element: PsiElement?): PsiElement? {
        var el = element;
        while (el != null) {
            if (isIt(el)) {
                return el;
            }

            el = el.parent;
        }

        return null;
    }

    private fun findChild(element: PsiElement?, name: String): PsiElement? {
        var el = element?.firstChild;
        while (el != null && el.elementType.toString() != name) {
            el = el.nextSibling;
        }

        return el;
    }

}
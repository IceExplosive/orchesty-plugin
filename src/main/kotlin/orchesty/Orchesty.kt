package orchesty

object Orchesty {

    val VALIDATE_DECORATOR = "validate";
    val INPUT_SCHEMA = "inputSchema";
    val INPUT_INTERFACE = "IInput";

}
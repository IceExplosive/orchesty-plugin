package orchesty;

import com.intellij.lang.Language;

public class OrchLanguage extends Language {

    public static final OrchLanguage INSTANCE = new OrchLanguage();

    private OrchLanguage() {
        super("Orchesty");
    }
}

package orchesty.form

import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import javax.swing.JComboBox

class TemplateSelector(private val selector: JComboBox<String>) : KeyAdapter() {

    override fun keyPressed(e: KeyEvent?) {
        val code = e?.keyCode;

        if (code != null) {
            if (code == KeyEvent.VK_UP) {
                val current = selector.selectedIndex - 1;
                selector.selectedIndex = if (current < 0) selector.itemCount -1 else current;
            } else if (code == KeyEvent.VK_DOWN) {
                val current = selector.selectedIndex + 1;
                selector.selectedIndex = if (current >= selector.itemCount) 0 else current;
            }
        }
    }

}
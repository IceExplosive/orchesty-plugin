package orchesty.utils

import com.intellij.lang.ecmascript6.psi.ES6ImportDeclaration
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.EditorModificationUtil
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.refactoring.suggested.endOffset

object TsImportUtils {

    fun addImportBinding(binding: String, importLine: String, editor: Editor, file: PsiFile) {
        val imports: Array<ES6ImportDeclaration>? = PsiTreeUtil.getChildrenOfType(file, ES6ImportDeclaration::class.java);
        var hasImport = false;
        if (imports != null) {
            for (import in imports) {
                for (iBinding in import.importedBindings) {
                    if (iBinding.name?.equals(binding) == true) {
                        hasImport = true;
                        break;
                    }
                }
                if (hasImport) {
                    break;
                }
            }
            if (!hasImport) {
                val el = imports.last() as PsiElement;
                editor.caretModel.moveToOffset(el.endOffset);
                EditorModificationUtil.insertStringAtCaret(editor, "\n$importLine");
            }
        }
    }

    // TODO tady by to chtělo vyřešit lépe, ale zatím takto stačí
    fun addNamedImport(name: String, importLine: String, editor: Editor, file: PsiFile) {
        val imports: Array<ES6ImportDeclaration>? = PsiTreeUtil.getChildrenOfType(file, ES6ImportDeclaration::class.java);
        var hasImport = false;
        if (imports != null) {
            for (import in imports) {
                for (specifier in import.namedImports?.specifiers ?: emptyArray()) {
                    if (specifier.declaredName == name) {
                        hasImport = true;
                        break;
                    }
                }
                if (hasImport) {
                    break;
                }
            }
            if (!hasImport) {
                val el = imports.last() as PsiElement;
                editor.caretModel.moveToOffset(el.endOffset);
                EditorModificationUtil.insertStringAtCaret(editor, "\n$importLine");
            }
        }
    }

}